from image_annotations import AnnotationDB
from collections import Counter
import os
import time
import pytest


def test_db(tmp_path):
    tmp_db_path = str(os.path.join(tmp_path, 'tmpdb.sqlite'))
    print(tmp_db_path)
    assert not os.path.exists(tmp_db_path)

    # create db
    db = AnnotationDB(tmp_db_path)
    filenames = ['a', 'b', 'c', 'd', 'e', 'f']
    db.create_db(filenames)

    returned_filenames = db.get_filenames()
    assert list(returned_filenames) == filenames

    assert db.has_file('a')
    assert not db.has_file('nonexistent_file')

    # get random filenames
    rand_files = Counter(db.get_random_filename() for i in range(1200))
    print(rand_files)
    assert set(rand_files.keys()) == set(filenames)
    for k, v in rand_files.items():
        assert 150 < v < 250

    # user history
    u1_hist = db.get_user_history(user='user1')
    assert len(u1_hist) == 0

    annotated, total, pos, neg = db.get_file_stats()
    assert annotated == 0
    assert total == len(filenames)
    assert (pos, neg) == (0, 0)

    # put some annotations
    start_timestamp = time.time()
    db.start_annotation('user1', 'a')
    table = db.get_annotation_table()
    assert len(table) == 1
    assert table[0][0] == 'a'
    assert table[0][1] == 'user1'
    assert table[0][2] == None
    assert table[0][4] >= start_timestamp
    assert table[0][5] == None

    u1_hist = db.get_user_history(user='user1')
    assert len(u1_hist) == 0

    annotated, total, pos, neg = db.get_file_stats()
    assert annotated == 0
    assert total == len(filenames)
    assert (pos, neg) == (0, 0)

    assert set(db.get_filenames()) == set(filenames)

    end_timestamp = time.time()
    db.end_annotation('user1', 'a', '1')
    table = db.get_annotation_table()
    assert len(table) == 1
    assert table[0][0] == 'a'
    assert table[0][1] == 'user1'
    assert table[0][2] == '1'
    assert table[0][4] >= start_timestamp
    assert table[0][5] >= end_timestamp

    u1_hist = db.get_user_history(user='user1')
    assert len(u1_hist) == 1
    assert u1_hist[0] == ('a', '1')

    annotated, total, pos, neg = db.get_file_stats()
    assert annotated == 1
    assert total == len(filenames)
    assert (pos, neg) == (1, 0)

    # check replacing
    start_timestamp = time.time()
    db.start_annotation('user1', 'a')
    table = db.get_annotation_table()
    assert len(table) == 1
    assert table[0][0] == 'a'
    assert table[0][1] == 'user1'
    assert table[0][2] == None
    assert table[0][4] >= start_timestamp
    assert table[0][5] == None

    end_timestamp = time.time()
    db.end_annotation('user1', 'a', '1')
    table = db.get_annotation_table()
    assert len(table) == 1
    assert table[0][0] == 'a'
    assert table[0][1] == 'user1'
    assert table[0][2] == '1'
    assert table[0][4] >= start_timestamp
    assert table[0][5] >= end_timestamp

    # add other annotations
    db.start_annotation('user2', 'a')
    table = db.get_annotation_table()
    assert len(table) == 2
    db.end_annotation('user2', 'a', '2')
    assert len(table) == 2

    db.start_annotation('user1', 'b')
    table = db.get_annotation_table()
    assert len(table) == 3
    db.cancel_annotation('user1', 'b')
    table = db.get_annotation_table()
    assert len(table) == 2

    # check replacing for start
    db.start_annotation('user1', 'b')
    table = db.get_annotation_table()
    assert len(table) == 3
    db.start_annotation('user1', 'b')
    table = db.get_annotation_table()
    assert len(table) == 3

    # put more annotations to check stats
    db.start_annotation('user1', 'b')
    db.end_annotation('user1', 'b', '1')
    db.start_annotation('user1', 'c')
    db.end_annotation('user1', 'c', '1')
    db.start_annotation('user2', 'b')
    db.end_annotation('user2', 'b', '1')
    assert len(db.get_annotation_table()) == 5

    # user stats
    user_counts = db.get_user_stats()
    assert len(user_counts) == 2
    assert user_counts[0][0:2] == ('user1', 3)
    assert user_counts[1][0:2] == ('user2', 2)
    assert 0 < user_counts[0][2] < 0.05
    assert 0 < user_counts[1][2] < 0.05

    db.start_annotation('user3', 'a')
    user_counts = db.get_user_stats()
    assert len(user_counts) == 2

    time.sleep(0.2)
    db.end_annotation('user3', 'a', '0')
    user_counts = db.get_user_stats()
    assert user_counts[2][0:2] == ('user3', 1)
    assert 0.2 < user_counts[2][2] < 0.3

    # get random filenames after annotations
    rand_files = Counter(db.get_random_filename() for i in range(1200))
    assert set(rand_files.keys()) == set(['d', 'e', 'f'])
    for k, v in rand_files.items():
        assert 350 < v < 450

    rand_files = Counter(db.get_random_filename(user='user3') for i in range(1000))
    assert set(rand_files.keys()) == set(['b', 'c', 'd', 'e', 'f'])
    for k, v in rand_files.items():
        assert 150 < v < 250

    rand_files = Counter(db.get_random_filename(exclude_annotated=False) for i in range(1200))
    assert set(rand_files.keys()) == set(filenames)
    for k, v in rand_files.items():
        assert 150 < v < 250

    # check empty random file
    db.start_annotation('user1', 'd')
    db.end_annotation('user1', 'd', '1')
    db.start_annotation('user1', 'e')
    db.end_annotation('user1', 'e', '1')

    rand_files = Counter(db.get_random_filename() for i in range(20))
    assert set(rand_files.keys()) == set(['f'])

    annotated, total, _, _ = db.get_file_stats()
    assert annotated == len(filenames) - 1
    assert total == len(filenames)

    db.start_annotation('user1', 'f')
    assert db.get_random_filename() == 'f'

    annotated, total, _, _ = db.get_file_stats()
    assert annotated == len(filenames) - 1
    assert total == len(filenames)

    db.end_annotation('user1', 'f', '1')
    assert db.get_random_filename() == None

    annotated, total, _, _ = db.get_file_stats()
    assert annotated == len(filenames)
    assert total == len(filenames)

    assert len(db.get_annotation_table()) == 9

    # check foreign key
    with pytest.raises(Exception) as e:
        db.start_annotation('user4', 'nonexistent_file')

    assert len(db.get_annotation_table()) == 9

    # close
    db.close()


def test_duplicate_files(tmp_path):
    tmp_db_path = str(os.path.join(tmp_path, 'tmpdb.sqlite'))
    print(tmp_db_path)
    assert not os.path.exists(tmp_db_path)

    db = AnnotationDB(tmp_db_path)
    with pytest.raises(Exception) as e:
        db.create_db(['a', 'b', 'c', 'a'])


def test_empty_files(tmp_path):
    tmp_db_path = str(os.path.join(tmp_path, 'tmpdb.sqlite'))
    print(tmp_db_path)
    assert not os.path.exists(tmp_db_path)

    db = AnnotationDB(tmp_db_path)
    with pytest.raises(Exception) as e:
        db.create_db([])


def test_stats(tmp_path):
    tmp_db_path = str(os.path.join(tmp_path, 'tmpdb.sqlite'))
    assert not os.path.exists(tmp_db_path)

    db = AnnotationDB(tmp_db_path)
    filenames = ['a', 'b', 'c', 'd', 'e', 'f']
    db.create_db(filenames)

    annotated, total, positive, negative = db.get_file_stats()
    assert (annotated, total) == (0, len(filenames))
    assert (positive, negative) == (0, 0)

    db.start_annotation('user1', 'a')
    db.end_annotation('user1', 'a', '1')
    annotated, total, positive, negative = db.get_file_stats()
    assert (annotated, total) == (1, len(filenames))
    assert (positive, negative) == (1, 0)

    db.start_annotation('user1', 'b')
    db.end_annotation('user1', 'b', '0')
    annotated, total, positive, negative = db.get_file_stats()
    assert (annotated, total) == (2, len(filenames))
    assert (positive, negative) == (1, 1)

    db.start_annotation('user1', 'c')
    db.end_annotation('user1', 'c', '1')
    annotated, total, positive, negative = db.get_file_stats()
    assert (annotated, total) == (3, len(filenames))
    assert (positive, negative) == (2, 1)

    db.start_annotation('user2', 'c')
    db.end_annotation('user2', 'c', '0')
    annotated, total, positive, negative = db.get_file_stats()
    assert (annotated, total) == (3, len(filenames))
    assert (positive, negative) == (2, 2)

    db.close()


def test_batch(tmp_path):
    tmp_db_path = str(os.path.join(tmp_path, 'tmpdb.sqlite'))
    assert not os.path.exists(tmp_db_path)

    db = AnnotationDB(tmp_db_path)
    filenames = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l']
    db.create_db(filenames)

    files = db.get_random_files_batch(size=4)
    assert len(files) == len(set(files)) == 4

    for f in filenames[4:]:
        db.start_annotation('u1', f)

    files = db.get_random_files_batch(size=10)
    assert len(files) == len(set(files)) == 10

    for f in filenames[4:]:
        db.end_annotation('u1', f, '1')

    files = db.get_random_files_batch(size=10)
    assert len(files) == len(set(files)) == 4
    assert set(files) == set(['a', 'b', 'c', 'd'])
