import sqlite3
import os
import time
from loguru import logger


class AnnotationDB:
    def __init__(self, db_path):
        self.db_path = db_path
        logger.info(f'open db at "{self.db_path}"')
        self.con = sqlite3.connect(self.db_path)

        # setup to enforce foreign key consistency
        self._exectute_sql('PRAGMA foreign_keys = 1')

    def start_annotation(self, user, filename):
        start_timestamp = time.time()
        sql = f'''
            INSERT OR REPLACE INTO annotations(filename, user, start_timestamp)
            VALUES("{filename}", "{user}", {start_timestamp});
        '''
        logger.info(f'start annotation: "{user}" on "{filename}" at ({start_timestamp}))')
        logger.debug(f'sql:{sql}')
        self._exectute_sql(sql)
        return self

    def end_annotation(self, user, filename, result):
        end_timestamp = time.time()
        sql = f'''
            UPDATE annotations
            SET result = "{result}",
                end_timestamp = {end_timestamp}
            WHERE
                filename = "{filename}" AND user = "{user}"
        '''
        logger.info(f'end annotation - "{result}" for "{user}" on "{filename}" at ({end_timestamp})')
        logger.debug(f'sql:{sql}')
        self._exectute_sql(sql)
        return self

    def cancel_annotation(self, user, filename):
        sql = f'DELETE FROM annotations WHERE user = "{user}" AND filename = "{filename}"'
        logger.info(f'deleting annotation for "{user}" on "{filename}"')
        logger.debug(f'sql: {sql}')
        self._exectute_sql(sql)
        return self

    def has_file(self, filename):
        rows = self._exectute_sql(f'SELECT * FROM filenames WHERE filename = "{filename}"').fetchall()
        assert len(rows) <= 1
        return len(rows) == 1

    def get_filenames(self):
        for row in self._exectute_sql('SELECT filename FROM filenames').fetchall():
            assert len(row) == 1
            yield row[0]

    def get_file_stats(self):
        total = self._exectute_sql('''SELECT COUNT(1) FROM filenames''').fetchall()[0][0]
        annotated = self._exectute_sql('''
            SELECT COUNT (DISTINCT filename)
            FROM annotations
            WHERE result NOT NULL
        ''').fetchall()[0][0]
        positive = self._exectute_sql('''SELECT COUNT(DISTINCT filename) FROM annotations WHERE result = "1"''').fetchall()[0][0]
        negative = self._exectute_sql('''SELECT COUNT(DISTINCT filename) FROM annotations WHERE result = "0"''').fetchall()[0][0]
        return annotated, total, positive, negative

    def get_annotation_table(self):
        return self._exectute_sql('''
            SELECT
                filename,
                user,
                result,
                (end_timestamp - start_timestamp) as duration,
                start_timestamp,
                end_timestamp
            FROM annotations;
        ''').fetchall()

    def get_user_history(self, user):
        sql = f'''
            SELECT filename, result
            FROM annotations
            WHERE user = "{user}" AND result NOT NULL
            ORDER BY end_timestamp ASC
        '''
        return self._exectute_sql(sql).fetchall()

    def get_user_stats(self):
        sql = f'''
            SELECT
                user,
                COUNT(filename) as annotation_count,
                AVG(end_timestamp - start_timestamp) as mean_duration
            FROM annotations
            WHERE end_timestamp NOT NULL
            GROUP BY user
            ORDER BY
                annotation_count DESC,
                mean_duration ASC
        '''
        logger.debug(f'sql:{sql}')
        return self._exectute_sql(sql).fetchall()

    def get_random_files_batch(self, size=24, user=None, exclude_annotated=True):
        assert size > 0

        if not exclude_annotated:
            sql = f'SELECT filename FROM filenames ORDER BY RANDOM() LIMIT 1'
        else:
            user_condition = '' if user is None else f'AND user = "{user}"'
            sql = f'''
                SELECT filenames.filename
                FROM filenames
                LEFT JOIN (
                  SELECT DISTINCT(filename) FROM annotations
                  WHERE result NOT NULL
                  {user_condition}
                ) AS annotated_files
                ON filenames.filename = annotated_files.filename
                WHERE annotated_files.filename IS NULL
                ORDER BY RANDOM()
                LIMIT {size}
            '''
        result = self._exectute_sql(sql).fetchall()
        filenames = [row[0] for row in result]
        return filenames

    def get_random_filename(self, user=None, exclude_annotated=True):
        maybe_filename = self.get_random_files_batch(size=1, user=user, exclude_annotated=exclude_annotated)
        assert 0 <= len(maybe_filename) <= 1
        if len(maybe_filename) == 1:
            return maybe_filename[0]
        else:
            return None


    def get_random_filename_old(self, user=None, exclude_annotated=True):
        if not exclude_annotated:
            sql = f'SELECT filename FROM filenames ORDER BY RANDOM() LIMIT 1'
        else:
            user_condition = '' if user is None else f'AND user = "{user}"'
            sql = f'''
                SELECT filenames.filename
                FROM filenames
                LEFT JOIN (
                  SELECT DISTINCT(filename) FROM annotations
                  WHERE result NOT NULL
                  {user_condition}
                ) AS annotated_files
                ON filenames.filename = annotated_files.filename
                WHERE annotated_files.filename IS NULL
                ORDER BY RANDOM()
                LIMIT 1
            '''
        filenames = self._exectute_sql(sql).fetchall()
        if len(filenames) == 0:
            return None
        elif len(filenames) == 1:
            return filenames[0][0]
        else:
            raise ValueError('expected at most one row, got more')

    def create_db(self, filenames, rewrite=False):
        assert len(filenames) >= 1, f'filenames empty "{filenames}"'

        create_filenames_sql = '''
            CREATE TABLE filenames (
                filename TEXT PRIMARY KEY
            );
        '''
        logger.info(f'create filenames:{create_filenames_sql}')
        self._exectute_sql(create_filenames_sql)

        filenames_idx_sql = '''
            CREATE UNIQUE INDEX idx_filenames ON filenames (filename)
        '''
        logger.info(f'create filenames index: {filenames_idx_sql}')
        self._exectute_sql(filenames_idx_sql)

        create_annotations_sql ='''
            CREATE TABLE annotations (
                filename TEXT,
                user TEXT,
                result TEXT,
                start_timestamp REAL,
                end_timestamp REAL,
                PRIMARY KEY (user, filename),
                FOREIGN KEY (filename) REFERENCES filenames(filename)
            );
        '''
        logger.info(f'create annotations: {create_annotations_sql}')
        self._exectute_sql(create_annotations_sql)

        annotations_idx_sql = '''
            CREATE UNIQUE INDEX idx_annotations ON annotations (user, filename)
        '''
        logger.info(f'create annotations index:{annotations_idx_sql}')
        self._exectute_sql(annotations_idx_sql)

        values_strings = ', '.join(f'("{f}")' for f in filenames)
        insert_sql = f'''
            INSERT INTO filenames
            VALUES
                {values_strings};
        '''
        logger.info(f'inserting {len(filenames)} values')
        logger.debug(f'sql: {insert_sql}')
        self._exectute_sql(insert_sql)

        return self

    def close(self):
        self.con.close()

    def _exectute_sql(self, sql):
        ret = self.con.execute(sql)
        self.con.commit()
        return ret
