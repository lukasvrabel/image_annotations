#!/usr/bin/env python
import click
import sys
import os
from loguru import logger
from image_annotations import AnnotationDB
from tabulate import tabulate


@click.command()
@click.option('--db', required=True, help='path to sqlite db file that will be listed')
def main(db):
    """list filenames and annotations from sqlite db"""
    # setup logger
    logger.remove()
    logger.add(sys.stderr, level='INFO')
    logger.info(f'opening db at "{db}"')

    if not os.path.exists(db):
        err_msg = f'no file at "{db}"'
        logger.error(err_msg)
        raise ValueError(err_msg)

    adb = AnnotationDB(db)
    print('filenames:')
    print(list(adb.get_filenames()))
    print()

    print(tabulate(
        adb.get_annotation_table(),
        headers=['filename', 'user', 'res', 'duration', 'start', 'end'],
        floatfmt=".1f"
    ))


if __name__ == '__main__':
    main()
