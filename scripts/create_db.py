#!/usr/bin/env python
import click
import sys
import os
from loguru import logger
from image_annotations import AnnotationDB

@click.command()
@click.option('--db', required=True, help='path to sqlite db file that will be created')
def main(db):
    """create sqlite db for annotations and populate it with filenames from stdin"""
    # setup logger
    logger.remove()
    logger.add(sys.stderr, level='INFO')
    logger.info(f'creating db at "{db}"')

    if os.path.exists(db):
        err_msg = f'file at "{db}" exists'
        logger.error(err_msg)
        raise ValueError(err_msg)

    filenames = [os.path.abspath(line.strip()) for line in sys.stdin]
    logger.info(f'read {len(filenames)} from stdin')
    adb = AnnotationDB(db)
    adb.create_db(filenames)
    logger.info('db created succesfully, bye')


if __name__ == '__main__':
    main()
