from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand


class PyTest(TestCommand):
    user_options = [("pytest-args=", "a", "Arguments to pass to pytest")]

    def initialize_options(self):
        TestCommand.initialize_options(self)
        self.pytest_args = ""

    def run_tests(self):
        import shlex
        import sys
        # import here, cause outside the eggs aren't loaded
        import pytest

        errno = pytest.main(shlex.split(self.pytest_args))
        sys.exit(errno)


setup(
    name='image_annotations',
    version='0.1',
    tests_require=['pytest==5.0.1'],
    cmdclass={"test": PyTest},
    install_requires=[
        'click==7.0',
        'flask==1.1.1',
        'loguru==0.3.0',
        'tabulate==0.8.3',
    ],
)
