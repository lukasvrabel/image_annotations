#!/usr/bin/env python
from flask import (
    Flask,
    escape,
    request,
    g,
    redirect,
    url_for,
    render_template,
    flash,
    jsonify,
    send_file,
    send_from_directory,
)
from image_annotations import AnnotationDB
from loguru import logger
import sys


app = Flask(__name__)
app.secret_key = b'annotations_secret_key'
app.config['DEBUG'] = True

db_path = sys.argv[1]


if True or not app.config['DEBUG']:
    logger.remove()
    logger.add(sys.stderr, level='INFO')


def get_db():
    db = getattr(g, '_db', None)
    if db is None:
        db = g._db = AnnotationDB(db_path)
    return db


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_db', None)
    if db is not None:
        logger.debug('closing db')
        db.close()


@app.route('/')
def index():
    username = request.cookies.get('username')
    return render_template('index.html', username=username)


@app.route('/annotate')
def annotate():
    username = request.cookies.get('username')
    if not username:
        return render_template('annotate.html')

    prev_filename = request.args.get('prev_filename')
    user_stats = get_db().get_user_stats()

    filename = request.args.get('filename')
    if filename == None:
        # try to get file with no annotations att all
        filename = get_db().get_random_filename()
        if filename == None:
            # try to get file with no annotation for current user
            filename = get_db().get_random_filename(user=username)

    if filename != None:
        get_db().start_annotation(username, filename)

    return render_template(
        'annotate.html',
        filename=filename,
        username=username,
        prev_filename=prev_filename,
        user_stats=user_stats,
    )


@app.route('/store_annotation', methods=['POST'])
def store_annotation():
    filename = request.form['filename']
    username = request.cookies.get('username')
    result = request.form['result']

    logger.info(f'got annotation "{result}" from "{username}" on "{filename}"')

    if result != 'don\'t know':
        get_db().end_annotation(username, filename, result)
        logger.info(f'stored')
        flash(f'stored "{result}" for "{filename}"')
    else:
        logger.info(f'skipped')
        flash(f'skipped "{filename}"')

    return redirect(url_for('annotate', prev_filename=filename))


@app.route('/cancel_annotation')
def cancel_annotation():
    username = request.cookies.get('username')
    filename = request.args.get('filename')

    if not username or not filename:
        logger.info(f'invalid cancle request for user "{username}" and file "{filename}"')
        flash(f'got invalid cancel request')
        return redirect(url_for('index'))

    logger.info(f'cancel annotation for "{username}" on "{filename}"')
    get_db().cancel_annotation(username, filename)
    flash(f'You were inactive too long, cancelled annotation for "{filename}"')

    return redirect(url_for('index'))


@app.route('/annotate_batch')
def annotate_batch():
    username = request.cookies.get('username')
    if not username:
        return render_template('annotate_batch.html')

    prev_filenames = request.args.getlist('prev_filename')

    user_stats = get_db().get_user_stats()

    filenames = request.args.getlist('filename')
    if not filenames:
        filenames = get_db().get_random_files_batch(size=10)

    db = get_db()
    for f in filenames:
        db.start_annotation(username, f)

    return render_template(
        'annotate_batch.html',
        filenames=filenames,
        prev_filenames=prev_filenames,
        username=username,
        user_stats=user_stats,
    )


@app.route('/store_annotation_batch', methods=['POST'])
def store_annotation_batch():
    username = request.cookies.get('username')
    if not username:
        flash('username not set')
        return render_template('annotate_batch.html')

    files_true = request.form.getlist('files_checked')
    files_all = request.form.getlist('files_all')
    files_false = set(files_all) - set(files_true)
    db = get_db()

    for f in files_true:
        db.end_annotation(username, f, '1')

    for f in files_false:
        db.end_annotation(username, f, '0')

    msg = f'stored batch annotation for "{username}" T/F {len(files_true)} / {len(files_false)}'
    logger.info(msg)
    flash(msg)

    return redirect(url_for('annotate_batch', prev_filename=files_all))


@app.route('/previous_batch', methods=['POST'])
def previous_batch():
    files = request.form.getlist('files_prev')
    flash('fixing previous batch')
    return redirect(url_for('annotate_batch', filename=files))


@app.route('/cancel_batch_annotation')
def cancel_batch_annotation():
    flash(f'You were inactive too long, cancelled annotation')
    return redirect(url_for('index'))


@app.route('/leaderboard')
def leaderboard():
    user_stats = get_db().get_user_stats()
    return render_template('leaderboard.html', user_stats=user_stats)


@app.route('/history')
def history():
    username = request.cookies.get('username')
    user_history = get_db().get_user_history(username)
    logger.info(f'user history: {len(user_history)} items')
    return render_template('history.html', username=username, history=user_history)


@app.route('/file_stats')
def file_stats():
    annotated, total, pos, neg = get_db().get_file_stats()
    return jsonify(result=f'{annotated} / {total} ({annotated / total*100:.1f}%) | pos:neg = {pos}:{neg}')


@app.route('/get_file')
def get_file():
    filename = request.args.get('filename')
    if get_db().has_file(filename):
        return send_file(filename)
    else:
        flash(f'requested file not database ("{filename}")')
        return send_from_directory('static', 'no_image.png')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=4444, debug=True)
